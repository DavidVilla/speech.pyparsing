#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import pyparsing as pp


def identifier():
    return pp.Word(pp.alphas)


def token(char):
    return pp.Suppress(char)


class ParserVisitor(object):
    def visitUnitStart(self, unitdef): pass
    def visitUnitEnd(self, unitdef): pass
    def visitModuleStart(self, module_def): pass
    def visitModuleEnd(self, module_def): pass
    def visitClassDecl(self, class_decl): pass
    def visitClassDefStart(self, class_def): pass
    def visitClassDefEnd(self, class_def): pass
    def visitExceptionStart(self, exception_def): pass
    def visitExceptionEnd(self, exception_def): pass
    def visitStructStart(self, struct_def): pass
    def visitStructEnd(self, struct_def): pass
    def visitOperation(self, operation_def): pass
    def visitParamDecl(self, param_decl): pass
    def visitDataMember(self, datamember): pass
    def visitSequence(self, sequence_def): pass
    def visitDictionary(self, dict_def): pass
    def visitEnum(self, enum_def): pass
    def visitConst(self, const_def): pass


class AST(list):
    def __init__(self):
        self.stack = [self]

    def top(self):
        return self.stack[-1]

    def add(self, node):
        self.top().append(node)

    def push(self, node):
        self.add(node)
        self.stack.append(node)

    def pop(self):
        self.stack.pop()


class Parser(object):
    def __init__(self, source):
        self.source = source
        self.ast = AST()

    def parse(self):
        self.grammar.parseString(self.source)

    def accept(self, visitor):
        for node in self.ast:
            node.accept(visitor)

    def module_def(self):
        name = identifier()
        name.addParseAction(self.module_def_action)
        start = pp.Keyword('module') + name + token('{')

        module = pp.Forward()
        item = module | self.interface_def()
        body = pp.ZeroOrMore(item)

        end = token('}') + token(';')
        end.addParseAction(self.ast.pop)

        module << start + body + end
        return module

    def module_def_action(self, source, pos, tokens):
        self.ast.push(Module(tokens[0]))

    def interface_def(self):
        name = identifier()
        name.addParseAction(self.interface_def_action)
        start = pp.Keyword('interface') + name + token('{')
        end = token('}') + token(';')
        end.addParseAction(self.ast.pop)
        return start + self.operation_seq() + end

    def interface_def_action(self, source, pos, tokens):
        self.ast.push(Interface(tokens[0]))

    def operation_seq(self):
        return pp.ZeroOrMore(self.operation_def())

    def operation_def(self):
        type_ = pp.Keyword('bool') | pp.Keyword('void') | pp.Keyword('int')
        param = pp.Group(type_ + identifier())
        params = pp.Optional(pp.delimitedList(param))
        operation = type_ + identifier() + token('(') + params + token(')') + token(';')
        operation.addParseAction(self.operation_def_action)
        return operation

    def operation_def_action(self, source, pos, tokens):
        params = [tuple(group) for group in tokens[2:]]
        self.ast.add(Operation(tokens[1], tokens[0], params))


class NamedContext(object):
    def __init__(self, name):
        self.name = name

    def __eq__(self, other):
        return self.name == other.name

    def __repr__(self):
        return "%s %s" % (self.__class__.__name__, self.name)


class Module(NamedContext, list):
    def accept(self, visitor):
        visitor.visitModuleStart(self)

        for item in self:
            item.accept(visitor)

        visitor.visitModuleEnd(self)

    def append(self, item):
        super(Module, self).append(item)


class Interface(NamedContext, list):
    def accept(self, visitor):
        for item in self:
            item.accept(visitor)

    def append(self, item):
        super(Interface, self).append(item)


class Operation(object):
    def __init__(self, name, return_type, params=None):
        self.name = name
        self.return_type = return_type
        self.params = params or []

    def accept(self, visitor):
        visitor.visitOperation(self)

    def __eq__(self, other):
        return self.name == other.name and self.return_type == other.return_type and \
            self.params == other.params

    def __repr__(self):
        return "operation %s %s (%s)" % (self.name, self.return_type, self.params)
