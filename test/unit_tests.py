# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
from unittest import TestCase
from doublex import ProxySpy, Spy, assert_that, called, ANY_ARG, is_, Mimic
from hamcrest import anything, all_of
from hamcrest.library.collection.issequence_containing import has_item, has_items

import pyparsing as pp

import sliceparser as sp


class TraceVisitor(sp.ParserVisitor):
    def visitModuleStart(self, module_def):
        print module_def


class SintacticTests(TestCase):
    def setUp(self):
        self.visitor = ProxySpy(TraceVisitor())

    def test_module(self):
        source = 'module DUO {};'

        parser = sp.Parser(source)
        parser.grammar = parser.module_def()
        parser.parse()
        parser.accept(self.visitor)

        assert_that(self.visitor.visitModuleStart,
                    called().with_args(sp.Module('DUO')))

    def test_interface(self):
        source = 'interface R { };'

        parser = sp.Parser(source)
        parser.grammar = parser.interface_def()
        module = Spy(sp.Module)
        parser.ast.push(module)

        parser.parse()

        assert_that(module.append,
                    called().with_args(sp.Interface('R')))

    def test_operation(self):
        source = 'bool get();'

        parser = sp.Parser(source)
        parser.grammar = parser.operation_def()
        module = Spy(sp.Module)
        parser.ast.push(module)

        parser.parse()

        operation = sp.Operation('get', 'bool')
        assert_that(module.append, called().with_args(operation))

    def test_operation_seq(self):
        source = '''\
bool get();
void ping();
'''

        parser = sp.Parser(source)
        parser.grammar = parser.operation_seq()
        interface = Spy(sp.Interface)
        parser.ast.push(interface)

        parser.parse()

        operation_get = sp.Operation('get', 'bool')
        assert_that(interface.append, called().with_args(operation_get))

        operation_set = sp.Operation('ping', 'void')
        assert_that(interface.append, called().with_args(operation_set))

    def test_operation_params(self):
        source = 'void set(bool value, int time);'

        parser = sp.Parser(source)
        parser.grammar = parser.operation_def()
        interface = Spy(sp.Interface)
        parser.ast.push(interface)

        parser.parse()

        operation = sp.Operation('set', 'void', [('bool', 'value'), ('int', 'time')])
        assert_that(interface.append, called().with_args(operation))

    def test_nested_modules(self):
        source = 'module DUO { module IBool {}; };'

        parser = sp.Parser(source)
        parser.grammar = parser.module_def()
        parser.parse()
        parser.accept(self.visitor)

        duo = sp.Module('DUO')
        ibool = sp.Module('IBool')
        assert_that(self.visitor.visitModuleStart, called().with_args(duo))
        assert_that(self.visitor.visitModuleStart, called().with_args(ibool))
        assert_that(self.visitor.visitModuleEnd, called().with_args(ibool))
        assert_that(self.visitor.visitModuleEnd, called().with_args(duo))

        assert_that(parser.ast[0].name, is_('DUO'))
        assert_that(parser.ast[0][0].name, is_('IBool'))

    def test_duo_bool_RW(self):
        source = '''\
module DUO {
    module IBool {
        interface R { bool get(); };
        interface W { void set(bool value); };
   };
};
'''

        parser = sp.Parser(source)
        parser.grammar = parser.module_def()
        parser.parse()
        parser.accept(self.visitor)

        duo = sp.Module('DUO')
        ibool = sp.Module('IBool')
        method_get = sp.Operation('get', 'bool')
        method_set = sp.Operation('set', 'void', [('bool', 'value')])
        assert_that(self.visitor.visitModuleStart, called().with_args(duo))
        assert_that(self.visitor.visitModuleStart, called().with_args(ibool))
        assert_that(self.visitor.visitOperation,
                    called().with_args(method_get))
        assert_that(self.visitor.visitOperation,
                    called().with_args(method_set))

        assert_that(self.visitor.visitModuleEnd, called().with_args(ibool))
        assert_that(self.visitor.visitModuleEnd, called().with_args(duo))

        assert_that(parser.ast[0].name, is_('DUO'))
        assert_that(parser.ast[0][0].name, is_('IBool'))
        assert_that(parser.ast[0][0][0].name, is_('R'))
        assert_that(parser.ast[0][0][0][0].name, is_('get'))
        assert_that(parser.ast[0][0][1].name, is_('W'))
        assert_that(parser.ast[0][0][1][0].name, is_('set'))
